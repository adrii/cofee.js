function dump(obj) {
    var out = '';
    for (var i in obj) {
        out += i + ": " + obj[i] + "\n";
    }
	
	var div = document.createElement('pre');
	div.style.position = 'absolute';
	div.style.top = "1%";
	div.style.right = "2%";
	div.style.border = "1px solid";
	div.innerHTML = out;
	document.body.appendChild(div);
}